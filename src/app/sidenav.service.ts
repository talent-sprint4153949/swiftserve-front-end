import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  collapsed = true;
  navData = [
    {
      routerLink:'dashboard',
      icon:'fal fa-home',
      label:'Dashboard'
    },
    {
      routerLink:'orders',
      icon:'fal fa-box-open',
      label:'Orders'

    }
  ];

  toggle() {
    this.collapsed = !this.collapsed;
  }

  close() {
      this.collapsed = false;
  }

  
}
