import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  

  isUserLoggedIn:boolean;
  isAdminLoggedIn:boolean;
  isCompanyLoggedIn:boolean;
  constructor(private http:HttpClient) {
    this.isUserLoggedIn=false;
    this.isAdminLoggedIn=false;
    this.isCompanyLoggedIn=false;
 
   }

   getStatus(){
    return this.isUserLoggedIn;
   }

  
  

  

  setUserLoggedIn() {
    this.isUserLoggedIn = true;
   
  }

  setAdminLoggedIn() {
    this.isAdminLoggedIn = true;
  
  }

  setCompanyLoggedIn() {
    this.isCompanyLoggedIn = true;
   
  }

  setUserLoggedOut() {
    this.isUserLoggedIn = false;
    
  }

  setAdminLoggedOut() {
    this.isAdminLoggedIn = false;
    
  }

  setCompanyLoggedOut() {
    this.isCompanyLoggedIn = false;
   
  }


   getAdmin(){
    return this.http.get("http://localhost:8080/getAdmin");
   }

   regCompany(companyData:any){
    return this.http.post("http://localhost:8080/getRegcompany",companyData,{responseType:'text'});
   }

   regClient(clientData:any){
    return this.http.post("http://localhost:8080/getRegCustomer",clientData,{responseType:'text'});
   }

   getCompany(){
    return this.http.get("http://localhost:8080/getAllCompany");
   }

   getClient(){
    return this.http.get("http://localhost:8080/getAllCustomer");
   }

   validateAdmin(email:any,password:any){
    return this.http.get("http://localhost:8080/validateAdmin/"+email+"/"+password,{responseType:'text'})
   }

   validate(email:any,password:any){
    return this.http.get("http://localhost:8080/validate/"+email+"/"+password,{responseType:'json'})
   }

   validateCustomer(email:any,password:any):Observable<any>{
    return this.http.get("http://localhost:8080/validateCustomer/"+email+"/"+password,{responseType:'json'})
   }

   delCompanyById(id:any){
    return this.http.delete("http://localhost:8080/delCompanyById/"+id,{responseType:'text'});
   }

   getRegCompany(){
    return this.http.get("http://localhost:8080/getAllRegCompany")
   }

   delRegCompanyById(id:any){
    return this.http.delete("http://localhost:8080/delRegCompanyById/"+id)
   }

   updateStatusByid(id:any){
    return this.http.put("http://localhost:8080/updateById/"+id,null,{responseType:'text'} )
   }

   validateCompanyStatus(email:any):Observable<any>{
    return this.http.get("http://localhost:8080/validateCompanyStatus/"+email,{responseType:'text'})
   }

   updateCompany(company:any){
     return this.http.put("http://localhost:8080/updateCompany",company,{responseType:'text'})
   }

   getServices()
{
  return this.http.get("http://localhost:8080/displayServices")
}

getServicesByCompanyId(id: string):Observable<any> { 
  return this.http.get<any>(`http://localhost:8080/getServicesByCompanyId/${id}`);
}

regServicesBycompId(servicesData:any,id:string)
{
  return this.http.post(`http://localhost:8080/regServicesByCompId/${id}`,servicesData,{responseType:'text'});
}

bookServices(bookingData:any)
{
    return this.http.post(`http://localhost:8080/postBookDetails`,bookingData,{responseType:'text'});
}

getCompanyByEmail(email:string){
  return this.http.get(`http://localhost:8080/getCompanyByEmail/${email}`);
}

delServicesById(servicesId:any){
  return this.http.delete(`http://localhost:8080/delServicesById/${servicesId}`,{responseType:'text'})
}


updateServices(services:any){
  return  this.http.put(`http://localhost:8080/updateServices`,services,{responseType:'text'})
}



getBookingBycustId(custId:any){
  return this.http.get(`http://localhost:8080/findBookingByCustId/${custId}`,{responseType:'text'})
}

cartItems(cartData:any,custId:string){
  return this.http.post( `http://localhost:8080/regCartbyCustId/${custId}`,cartData,{responseType:'text'})

}

getCartItems(custId: any) {
  return this.http.get(`http://localhost:8080/findCartBycustId/${custId}`);
}

delCartItems(cartId:any){
  return this.http.delete(`http://localhost:8080/delCartItemsbyid/${cartId}`,{responseType:'text'})
}


// forgot password 

getUserByEmail(email:any){
  return this.http.get(`http://localhost:8080/findByEmail/${email}`)
}


sendOtpToEmail(email:any){
  return this.http.get(`http://localhost:8080/sendOtpEmail/${email}`)
}

updateCustomerPassword(customer:any){
  return this.http.post(`http://localhost:8080/updateCustomerPassword`,customer)
}


}
