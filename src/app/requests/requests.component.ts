import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BookingService } from '../booking.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrl: './requests.component.css'
})
export class RequestsComponent  implements OnInit {
  p: number = 1; 
  itemsPerPage: number = 10; 
  pagedCompanies: any[] = []; 

  companys:any;
  constructor(private service:BookingService,private http:HttpClient,private toastr:ToastrService,private router:Router){}

  ngOnInit(): void {
    this.service.getCompany().subscribe((data)=>{
    this.companys=data;
    this.setPage(1);

    })
  } 

  setPage(page: number) {
   
    let startIndex = (page - 1) * this.itemsPerPage;
    let endIndex = Math.min(startIndex + this.itemsPerPage - 1, this.companys.length - 1);
    this.pagedCompanies = this.companys.slice(startIndex, endIndex + 1);
    this.p = page; 
  }

  pageChanged(event: any) {
    this.setPage(event); 
  }

  approveCompany(id:any){
    this.service.updateStatusByid(id).subscribe(()=>{
      this.service.getCompany().subscribe((data)=>{
        this.companys=data;
        this.toastr.success("Company Approved Successfully")
        this.router.navigate(['/home'])
      })
    })

  }

  deleteCompany(id:any){
    this.service.delCompanyById(id).subscribe(()=>{
      this.companys.filter((company:any)=>company.id !==id);
      this.toastr.success("company deleted Successfully");
      this.router.navigate(['/home']);

    })
  }

}
