import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookingService } from '../booking.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';

declare var jQuery:any;

@Component({
  selector: 'app-myservices',
  templateUrl: './myservices.component.html',
  styleUrls: ['./myservices.component.css']
})
export class MyservicesComponent implements OnInit {

  retrieved:any;
  services: any; 
  serv:any;
  servToUpdate = {imageUrl:'',companyName:'',description:'',mobileNumber:'',servicePrice:'',technicianName:''};
  

  constructor(private service: BookingService, private router: Router,private toastr:ToastrService,private http:HttpClient) {
    this.serv={serviceId:'',imageUrl:'',companyName:'',description:'',mobileNumber:'',servicePrice:'',technicianName:''}
  }

  ngOnInit() {
    const id = localStorage.getItem('id');
    if (id) {
      this.service.getServicesByCompanyId(id).subscribe((data: any) => {
        this.services = data;
      });
    } else {
      
     console.log(this.services.id);
     
    }

    this.service.getServices().subscribe((data)=>{
      this.services=data;
    })

    
  }

  editServices(serv:any) {
    this.servToUpdate=serv;
    
  }

  deleteservices(servicesId:any){
    this.service.delServicesById(servicesId).subscribe(()=>{
      this.services.filter((serv:any)=>serv.servicesId !=servicesId);
      this.toastr.success("Service Deleted Succesfully");
      this.router.navigate(['/home']);
    })
  }

  updateService(){
this.service.updateServices(this.servToUpdate).subscribe((data)=>{
      this.toastr.success("Service Updated Succesfully");
     
        
      })
  }
}