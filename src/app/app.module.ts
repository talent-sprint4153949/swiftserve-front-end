import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SignupClientComponent } from './signup-client/signup-client.component';
import { SignupCompanyComponent } from './signup-company/signup-company.component';
import { FormsModule, NgModel, } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { ShowallClientsComponent } from './showall-clients/showall-clients.component';
import { ToastrModule } from 'ngx-toastr';
import { RequestsComponent } from './requests/requests.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PaymentComponent } from './payment/payment.component';
import { ServicesComponent } from './services/services.component';
import { CartComponent } from './cart/cart.component';
import { YourOrdersComponent } from './your-orders/your-orders.component';

import { FooterComponent } from './footer/footer.component';
import { MyservicesComponent } from './myservices/myservices.component';
import { AddServicesComponent } from './add-services/add-services.component';
import { AboutComponent } from './about/about.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ForgopasswordComponent } from './forgopassword/forgopassword.component';
import { NgxPaginationModule } from 'ngx-pagination';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    SignupClientComponent,
    SignupCompanyComponent,
    HomeComponent,
    ShowallClientsComponent,
    RequestsComponent,
    PaymentComponent,
    ServicesComponent,
    CartComponent,
    YourOrdersComponent,
    FooterComponent,
    MyservicesComponent,
    AddServicesComponent,
    AboutComponent,
    ForgopasswordComponent,
   
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut:1000,
      positionClass:'toast-top-right',
      preventDuplicates:true,
      closeButton:true,
      progressAnimation:'decreasing',
      onActivateTick:false
     
    }),
    NgxCaptchaModule,
    NgxWebstorageModule.forRoot(),
    NgxPaginationModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
