// cart.component.ts
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BookingService } from '../booking.service';

declare var Razorpay:any;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
cartItems:any;
custId:any;
cart:any;
cartId:any;
   
cartData:any;

bookServe={
   
  serName:'',
  price:'',
  date:'',
  adress:'',
  number:''

}
constructor(private service:BookingService,private router:Router,private toastr :ToastrService,private http:HttpClient){

}
    
ngOnInit() {
 this.custId=localStorage.getItem('id');
 if(this.custId){
  this.service.getCartItems(this.custId).subscribe((data:any)=>{
    this.cartItems=data;
  });

 }else{
  this.toastr.warning("cart Items not added")
 }
}

deletecartIems(cartId:any){
  this.service.delCartItems(cartId).subscribe(()=>{
    this.cartItems.filter((cart:any)=>cart.cartId!=cartId);
    this.toastr.success("cart Item removed SuccesFully");
    this.router.navigate(['/home'])

  })
}

book(cart: any) {
  this.bookServe = { ...cart };
}

bookservices(custId:any){
  const RazorpayOptions={
    currency:'INR',
    amount:this.bookServe.price,
    key:'rzp_test_A475cK1t4w7tcw',

    theme:{
      color:'#0c238a'
    },
    modal:{
      ondismiss:()=>{
        this.toastr.warning("payment Cancelled");
        this.service.getBookingBycustId(custId).subscribe((data:any)=>{
          this.toastr.success("Booking Successfull");
        })
      }
    }
  }

  const successCallback=(paymentid:any)=>{
    this.toastr.success('Payment Successfull');
   this.registerBooking(this.custId);
   this.deletecartIems(this.cartId)
  
   
  }

  const errorCallback=(error:any)=>{
    this.toastr.error('Payment Failed');
  }

  Razorpay.open(RazorpayOptions,successCallback,errorCallback)

  
  
}


registerBooking(custId:any){
  const url =`http://localhost:8080/regBookbyCustId/${custId}`;
  this.http.post(url,this.bookServe).subscribe((response)=>{
    this.toastr.success("Booking Successfull")
  },
(error)=>{
  this.toastr.error("Booking Failed")
})
  }
  

  
  

 
 
}
