import { Component } from '@angular/core';
import { BookingService } from '../booking.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { error } from 'jquery';

@Component({
  selector: 'app-forgopassword',
  templateUrl: './forgopassword.component.html',
  styleUrl: './forgopassword.component.css'
})
export class ForgopasswordComponent {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

  generatedOtp:any;
  otpSent:boolean =false;
  openResetPassword:boolean =false;
  user:any;
  customer:any
  closeForgotForm:boolean=false;

  constructor(private service:BookingService,private toastr:ToastrService,private router:Router){
    this.user={
      fullName:'',
      mobile:'',
      email:'',
      password:'',
    }
  }

  forgotPassword(email:any){
    console.log(email);
    this.service.getUserByEmail(email).subscribe((data:any)=>{
      this.user=data;
    })
    
  }

  forgotpassword(forgotPasswordForm:NgForm) {
    const email = forgotPasswordForm.value.email;
    localStorage.getItem('email');
    this.service.sendOtpToEmail(email).subscribe((data: any) => {
        this.generatedOtp = data;
        this.otpSent = true;
        console.log('Otp Sent !!!..');
        this.toastr.success('Otp Sent to your mail !!!..');
    });
  }
  

   otpSubmit(otp:any){
    if(this.generatedOtp == otp){
      this.closeForgotForm=true;
      this.openResetPassword=true;
    }
   }

   resetPassword(newPasswordForm:any) {
   if(newPasswordForm.valid){
    const email = newPasswordForm.value.email;
    if(email){
      const password = newPasswordForm.value.password;
      this. customer ={'email':email,'password':password} 
      this.service.updateCustomerPassword(this.customer).subscribe((data)=>{
     this.toastr.success("password Updated Succesfully");
     this.router.navigate(['/home']);
      }
      
    )
    }(error:any)=>{
 this.toastr.warning("Password Update failed");
    }
   }
}



  

   }                                

      
      
  


