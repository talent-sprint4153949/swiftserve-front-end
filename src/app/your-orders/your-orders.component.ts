import { Component, OnInit } from '@angular/core';
import { BookingService } from '../booking.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-your-orders',
  templateUrl: './your-orders.component.html',
  styleUrl: './your-orders.component.css'
})
export class YourOrdersComponent implements OnInit {

  order: any;
  Orders: any;

  constructor(private service: BookingService, private http: HttpClient) {
  
  }

  ngOnInit(): void {
    const custId = localStorage.getItem('id');
    if (custId) {
      this.service.getBookingBycustId(custId).subscribe((response)=>{
        this.Orders=JSON.parse(response)
      })
    }
  }

  


}
