import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupClientComponent } from './signup-client/signup-client.component';
import { SignupCompanyComponent } from './signup-company/signup-company.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ShowallClientsComponent } from './showall-clients/showall-clients.component';
import { RequestsComponent } from './requests/requests.component';
import { PaymentComponent } from './payment/payment.component';
import { ServicesComponent } from './services/services.component';
import { CartComponent } from './cart/cart.component';

import { YourOrdersComponent } from './your-orders/your-orders.component';
import { AboutComponent } from './about/about.component';
import { MyservicesComponent } from './myservices/myservices.component';
import { AddServicesComponent } from './add-services/add-services.component';
import { ForgopasswordComponent } from './forgopassword/forgopassword.component';


const routes: Routes = [
  {path:'home',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'reg-client',component:SignupClientComponent},
  {path:'reg-company',component:SignupCompanyComponent},
  {path:'showcompany',component:ShowallClientsComponent},
  {path:'request',component:RequestsComponent},
  {path:'payment',component:PaymentComponent},
  {path:'services',component:ServicesComponent},
  {path:'cart',component:CartComponent},
  {path:'orders',component:YourOrdersComponent},
  {path:'home',component:HomeComponent},
  {path:'about',component:AboutComponent},
  {path:'myservices',component:MyservicesComponent},
  {path:'addservice',component:AddServicesComponent},
  {path:'forgot',component:ForgopasswordComponent},


  {path:'',redirectTo:'/login',pathMatch:'full'}

 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
