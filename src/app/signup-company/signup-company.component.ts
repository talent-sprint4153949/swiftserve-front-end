import { Component, OnInit } from '@angular/core';
import { BookingService } from '../booking.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup-company',
  templateUrl: './signup-company.component.html',
  styleUrl: './signup-company.component.css'
})
export class SignupCompanyComponent implements OnInit {
 siteKey:string;
 captchaCompleted = false;
  constructor(private service:BookingService,private http:HttpClient,private router:Router,private toastr :ToastrService){
    this.siteKey = '6LdaVagpAAAAAESiXy26lahakgLzzgVnjKi5prmf'
  
  }

  ngOnInit(): void {
    
  }
    regCompany(regforClientForm:any){
      this.service.regCompany(regforClientForm.value).subscribe((result:any)=>{
        console.log("Registration Sucessfull",result);
        this.toastr.success("Registration Request sent to Admin");
        this.router.navigate(['/login']);
        return ;
        
      },
      (error:any)=>{
        this.toastr.error("Registration Failed");
        console.log("Registration Failed",error);
      }
    )
    }
   
}
