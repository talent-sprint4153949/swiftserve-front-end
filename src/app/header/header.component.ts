import { Component } from '@angular/core';
import { BookingService } from '../booking.service';
import { ToastrService } from 'ngx-toastr';
import { SidenavService } from '../sidenav.service';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {

  services:any;
  
  constructor(public service:BookingService,private toastr:ToastrService,public sidenavService:SidenavService,private local:LocalStorageService){}

  logout(){
    this.local.clear('status');
    this.local.clear('cstatus');
    this.local.clear('customerstatus')
    this.service.setUserLoggedOut();
    this.service.setAdminLoggedOut();
    this.service.setCompanyLoggedOut();
    this.toastr.success('Logged Out Successfully');
    this.services=[];
   
  }

  toggleCollapse() {
    this.sidenavService.toggle();
  }

  collapsed = this.sidenavService.collapsed;


 
}
