import { Component, OnInit } from '@angular/core';
import { BookingService } from '../booking.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

declare var jQuery:any;
@Component({
  selector: 'app-showall-clients',
  templateUrl: './showall-clients.component.html',
  styleUrl: './showall-clients.component.css'
})
export class ShowallClientsComponent  implements OnInit {
  p: number = 1; 
  itemsPerPage: number = 10; 
 
  pagedCompanies: any[] = []; 
  company:any;
  companys:any;
  constructor(private service:BookingService,private http:HttpClient,private toastr:ToastrService,private router:Router){
    this.company = {id:'',email:'',name:'',number:''};
  }

  ngOnInit(): void {
    this.service.getCompany().subscribe((data)=>{
    this.companys=data;
    this.setPage(1);
    })
  } 
  

  

  deleteCompany(id:any){
    this.service.delCompanyById(id).subscribe(()=>{
      this.companys.filter((company:any)=>company.id !==id);
      this.toastr.success("company deleted Successfully");
      this.router.navigate(['/home']);

    })
  }

  setPage(page: number) {
   
    let startIndex = (page - 1) * this.itemsPerPage;
    let endIndex = Math.min(startIndex + this.itemsPerPage - 1, this.companys.length - 1);
    this.pagedCompanies = this.companys.slice(startIndex, endIndex + 1);
    this.p = page; 
  }

  pageChanged(event: any) {
    this.setPage(event); 
  }

}
