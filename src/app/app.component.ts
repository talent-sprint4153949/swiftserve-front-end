import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'ngx-webstorage';
import { ServicesComponent } from './services/services.component';
import { BookingService } from './booking.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent  implements OnInit{
  title = 'bookingsystem';
  constructor(private service:BookingService,private toastr:ToastrService,private local:LocalStorageService,private router:Router){
    
  }

  ngOnInit(): void {
    if(this.local.retrieve('status')){
      this.service.setAdminLoggedIn();
      this.router.navigate(['/home'])
    }else if(this.local.retrieve('cstatus')){
      this.service.setCompanyLoggedIn();
      this.router.navigate(['/home']);
    }else if(this.local.retrieve('customerstatus')){
      this.service.setUserLoggedIn();
      this.router.navigate(['/home']);
    }
  }

  


  

}
