import { Component } from '@angular/core';
import { BookingService } from '../booking.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup-client',
  templateUrl: './signup-client.component.html',
  styleUrl: './signup-client.component.css'
})
export class SignupClientComponent {
  
  siteKey:string;
 captchaCompleted = false;

constructor(private service:BookingService,private router:Router,private toastr:ToastrService){
  this.siteKey = '6LdaVagpAAAAAESiXy26lahakgLzzgVnjKi5prmf'
}
  regClient(regforClientForm:any){
    if(regforClientForm.valid){
      this.service.regClient(regforClientForm.value).subscribe((result:any)=>{
        console.log("Registration Sucessfull",result);
        this.toastr.success("Registered Successfully");
        this.router.navigate(['/login']);

      },
      (error:any)=>{
        this.toastr.error("Registration Failed");
        console.log("Registration Failed",error);
      })
    }
  
  }
}
