import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

declare var Razorpay:any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrl: './payment.component.css'
})
export class PaymentComponent {

constructor(private toastr:ToastrService){}

paynow(){
  const RazorpayOptions={
    description:"sample Razorpay deemo",
    currency:'INR',
    amount:30000,
    name:'tharun',
    key:'rzp_test_0259qs2vgzqAfP',
    prefill:{
      name:'Tharun',
      email:'tharun@gmail.com',
      phone:'9876543210'
    },
    theme:{
  color:'#0c238a'
    },
    modal:{
      ondismiss:()=>{
        this.toastr.warning("payment cancelled");
      }
    }

  }
  const successCallback = (paymentid: any) => { 
    this.toastr.success("payment successfull");
  };

  const errorCallback = (error: any) => { 
   this.toastr.error("payment failed");
  };

  Razorpay.open(RazorpayOptions, successCallback, errorCallback); 
}

  
}


  
